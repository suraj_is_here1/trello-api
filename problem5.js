const getList = require("./problem3");
const getCards = require("./problem4");


function getAllCards(boardId) {
    return getList(boardId)
        .then((listData) => {
            let listIds = listData.map((list) => list.id)

            return Promise.all(listIds.map(listId => getCards(listId)))
        })
}

module.exports = getAllCards;