const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";

function updateAllCheckItems(boardId) {
    return getAllCheckList(boardId)
        .then((checkListData) => {
            // console.log(checkListData);
            let checkListIdArray = checkListData.reduce((prevData, currData) => {
                let newCurrData = [];
                newCurrData.push(currData.id);
                newCurrData.push(currData.idCard);
                prevData.push(newCurrData);
                return prevData;
            }, []);
            return checkListIdArray;
        })
        .then((checkListIdArray) => {
            let checkListPromises = checkListIdArray.map((checkListDataId) => getCheckItemsData(checkListDataId));

            return Promise.all(checkListPromises);

        })

}

function getAllCheckList(boardId) {
    return fetch(
        `https://api.trello.com/1/boards/${boardId}/checklists?key=${APIKey}&token=${APIToken}`,
        {
            method: "GET",
        }
    ).then((response) => {
        return response.json();
    })

}

function getCheckItemsData(checkListId) {
    return fetch(
        `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${APIKey}&token=${APIToken}`,
        {
            method: "GET",
        }
    )
        .then((response) => {
            return response.json();
        })
        .then((checkItemData) => {
            let checkAllItemsPromise = checkItemData.map((data, index) => {
                if (index == 0) {
                    checkItemsAll(data.id, checkListId[1]);
                }
                else {
                    setTimeout(() => {
                        checkItemsAll(data.id, checkListId[1]);
                    }, 1000 * index);
                }


            });
            return Promise.all(checkAllItemsPromise);
        })

}


function checkItemsAll(checkItemDataId, cardId) {

        return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemDataId}?state=incomplete&key=${APIKey}&token=${APIToken}`, {
            method: 'PUT'
        })
            .then(response => {
                return response.json();
            })
}


module.exports = updateAllCheckItems