const deleteList = require("./problem7");

const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";


function deleteListSequentially(listIds, index) {
    return new Promise((res, rej) => {
        if (!listIds) {
            rej("Pass valid ListIds !")
        }
        if (index < listIds.length) {
            deleteList(listIds[index])
                .then((deleteMsg) => {
                    console.log(deleteMsg)
                    res(deleteListSequentially(listIds, index + 1))
                })
        }
        else {
            res("All list Deleted sequentially !");
        }

    })



}

module.exports = deleteListSequentially;