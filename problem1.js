const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";

function getBoard(boardId) {
    let url = `https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${APIToken}`;

    return fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            return response.json();
        })
}

module.exports = getBoard;