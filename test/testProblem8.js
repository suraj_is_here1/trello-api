const getList = require("../problem3");
const deleteListSequentially = require("../problem8");


let boardId = "66333b12f8ed3cf26ddc2a71";
getList(boardId)
    .then(listData => {
        listIds = listData.map((data) => data.id);
        deleteListSequentially(listIds, 0).then(console.log)
    })
    .catch((err => console.log("Unexpected error encountered !")))