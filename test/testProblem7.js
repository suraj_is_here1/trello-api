const deleteList = require("../problem7");
const getList = require("../problem3");

let boardId = "1KvEfPkF";
getList(boardId)
    .then(listData => {
        const listPromise = listData.map((data) => deleteList(data.id));
        return Promise.all(listPromise);
    })
    .then(deleteMsg => console.log(deleteMsg))
    .catch(() => console.log("Unexpected error encountered !"));