const createBoard = require("./problem2");
const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";

function multipleTask(boardName, NumberOfList) {

    return createBoard(boardName)
        .then(data => {
            console.log("Board Created Successfully !");
            let listPromise = [];
            let boardId = data.id;
            for (let count = 1; count <= NumberOfList; count++) {
                listPromise.push(createList(`list ${count}`, boardId)
                )
            }
            return Promise.all(listPromise);
        })
        .then(listData => {
            console.log("List created successfully !");
            let cardPromise = listData.map((list) => createCard('MyCard', list.id))
            Promise.all(cardPromise);
            console.log("Card created successfully !");
        })
        .catch(() => console.log("Unexpected error encountered !"))
}

function createList(listName, boardId) {

    let url = `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${APIKey}&token=${APIToken}`
    return fetch(url, {
        method: 'POST'
    })
        .then(response => {
            return response.json();
        })
}

function createCard(cardName, listId) {
    let url = `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${APIKey}&token=${APIToken}`

    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            return response.json();
        })

}

module.exports = multipleTask;