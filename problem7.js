const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";

function deleteList(listId) {
  return new Promise((resolve, reject) => {
    if (!listId) {
      reject("No list has been created yet.");
      return;
    }
    let url = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${APIKey}&token=${APIToken}`;
    fetch(url, {
      method: 'PUT'
    })
      .then(() => {
        resolve(`List with ID ${listId} has been deleted.`);
      })
      .catch(error => {
        reject(error);
      });
  });
}
module.exports = deleteList;