const APIToken = "ATTAdfee9f157613f2f97c8d114f8089cffbdbcc178c2b689aa4a20604cdb6f337c37B234716";
const APIKey = "25ebf0090cc5ced7129348c91b1debb6";
const permission = "public";

function createBoard(boardName) {
    let url = `https://api.trello.com/1/boards/?name=${boardName}&prefs_permissionLevel=${permission}&defaultLists=false&key=${APIKey}&token=${APIToken}`

    return fetch(url, {
        method: 'POST'
    })
        .then(response => {
            return response.json();
        })
}
module.exports = createBoard;
